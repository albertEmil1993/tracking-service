import socketEvents from '../src/socketEvents';



module.exports = {
    tracking: function (io) {
        global.trackingNSP = io.of('/tracking');
        trackingNSP.on('connection', async function (socket) {
            var id,roomName;
            socket.on(socketEvents.join, async function (data) {
                id = data;
                console.log('client  ' + id);
                roomName = 'room-' + id;
                socket.join(roomName);
                console.log('*******************' + roomName);
            });
         
            socket.on(socketEvents.select, async function (data) {
                console.log('////////////////////////' + data);
                console.log('select++++++++++++'+roomName);
                trackingNSP.to(roomName).emit(socketEvents.select, { data });
            });
            socket.on(socketEvents.location, async function (data) {
                console.log('////////////////////////' + data);
                trackingNSP.emit(socketEvents.location, { data });
            });
        })
    },

}

